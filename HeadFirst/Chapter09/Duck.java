public class Duck extends Animal
{
    int size;
    public Duck()
    {
        System.out.println("Making a duck. ");
    }

    public Duck(String name)
    {
        super(name);
        System.out.println("Making a duck with a name. ");
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public int getSize()
    {
        return size;
    }
}
