public class UseADuck
{
    public static void main(String args[])
    {
        Duck d = new Duck("sally");
        System.out.println(d.getName());

        Duck d2 = new Duck();
        d2.setSize(14);
        System.out.println(d2.getSize());
        d2 = null;
        System.out.println(d2.getSize());
    }
}
