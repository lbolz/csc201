public class Animal
{
    private String name;

    public Animal() {
        System.out.println("Making an animal");
    }

    public Animal(String theName)
    {
        this.name = theName;
    }

    public String getName()
    {
        return name;
    }

}
