# notes
- a GUI is a graphical interface
- the JFrame is an object that represents the GUI
- you can create a sub-class of JPanel that allows you to make your own widget in order to put it on a canvas

## buttons
- can easily create a button using the command:
``` java
frame.getContentPane().add(button);
```
- in order to make the button do something, you need two things:
    - a method for the user result, known as an Event
    - a way to know when to make that method run
        - this is called event-handling
        - you can do this by implementing a listener interface that listens for when the event gets called

## listeners/events
- the listener gets the event
    - implements the interface
    - registers the button
    - provides event-handling
- the source sends the event
    - accepts registrations from the listeners
    - gets events from the user (so when the user clicks a button)
    - performs event-handling
- event object
    - holds the event data inside of it

## GUI stuff
### about paintComponent() and JPanel
- all graphics code should go inside of a file that inherits the JPanel class, and should override the paintComponent() method
    - look to MyDrawPanel.java and SimpleGui3C.java for examples of this
    - never call paintComponent(), the system must do it because we cannot fill out the arguments
    - instead, ask the system to repaint(), and then paintComponent() will be called
- the argument of paintComponent() is a Graphics2D object, but the reference type is of a Graphic object, so you can cast it to a Graphics2D object if needed, like such:
``` java
Graphics2D g2d = (Graphics2D) g;
```

### using paintComponent()
- there are several ways to include things into a GUI, all of which should be inside of the paintComponent() method
    - putting widgets in a frame
    ``` java
    frame.getContentPane().add(myButton);
    ```
    - drawing 2D widgets on a widget
    ``` java
    graphics.fillOval(70, 70, 100, 100);
    ```
    - putting a JPEG into a widget
    ``` java
    graphics.drawImage(myPic, 10, 10, this);
    ```
## GUI layouts
- a frame has 5 regions where you can put things:
    - north
    - west
    - east
    - south
    - center
- you can only put one thing in each region
- for example, the example below puts a button in the center region of the frame
``` java
frame.getContentPane().add(BorderLayout.CENTER, button);
```
## class-ception
- you can have a inner class inside of an outer class
- an inner class can use the methods and variables the outer class, even if the outer class' variables are private
- to use the inner class with the outer class, you must instantiate it after instantiating the outer class to link the classes, as shown below:
``` java
class MyOuter {
     private int x;
     MyInner inner = new MyInner();

     public void doStuff() { inner.go(); }
     // ^ this only works because you instantiated the inner class

     class MyInner
     {
         void go() { x = 42; }
     }
}
```
