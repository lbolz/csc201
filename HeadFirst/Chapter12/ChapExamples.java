import javax.swing.*;

public class ChapExamples
{
    // in the book, this is the SimpleGui1 class example
    public static void main(String args[])
    {
        JFrame frame = new JFrame();
        JButton button = new JButton("click me");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(button);
        frame.setSize(400, 300);
        frame.setVisible(true);
    }
}
