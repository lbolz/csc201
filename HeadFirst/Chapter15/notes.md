# notes
- a socket represents the connection between two things: the client and the server
- it handles how the two of them send data between each other
- to make a socket, you need two things: what the server is, and what port it's on
    - these are called: the IP address, and the TCP port number

``` java
Socket chatSocket = new Socket("196.164.1.103", 5000);
// the 196.164... is the ip address, and the 5000 is the port number
```
- ports are unique identifiers that range from 0-65536
- the TCP port numbers form 0-1023 are reserved for well known servers/services, so don't use those, but you can use any other ones
- you need to make sure the port number matches the server, or the server won't know what to do with the data

## reading data from sockets
- to read data from a socket, you need to use a buffered reader
- example of reading data:
``` java
// make a socket connection
Socket chatSocket = new Socket("127.0.0.1", 5000);
// make an InputStreamReader chained to the Socket’s low-level (connection) input stream
InputStreamReader stream = new InputStreamReader(chatSocket.getInputStream());
// make a BufferedReader and read
BufferedReader reader = new BufferedReader(stream);
String message = reader.readLine();
```

## writing data to a socket
- to write data to a socket, you need to use a print writer
    - this is because the print writer can take in a whole string at a time
``` java
// make a socket connection to the server - just like when you write it
Socket chatSocket = new Socket("127.0.0.1", 5000);
// chain a print writer to the socket
PrintWriter writer = new PrintWriter(chatSocket.getOutputStream());
// write something, just like using System.out.print("...")
writer.println("message to send");
writer.print("another message");
```

## writing a server
- you need two sockets to do this
    - a ServerSocket, which waits for client requests
    - a regular socket to communicate directly with the client
- how it works
    - the server makes a ServerSocket
    - the client makes a socket connection and connects to the server
    - the server makes a new socket to communicate with the client

## threading and multithreading

### what's a thread
- a thread is a single thread of execution (as in, you can do one thing at a time), which we've been calling the "call stack"
- the JVM is responsible for creating the main thread, but sometimes that's not enough, so...

### how to multithread
- you can multithread- do multiple things at once- as in, make another call stack
- in java, this is done by creating a new thread object:
``` java
// this thread comes from java.lang, which is already implicitly imported
Thread t = new Thread();
t.start();
```
- but this thread doesn't do anything- it just exists
- to give a thread functionality, you need to give it a job (described in more detail [here](#Runnable))
- so, multithreading in java means you need to consider two factors for each thread: the thread and the job that is run by the thread
- when you have multiple stacks, the JVM appears to be running multiple things at once, but it isn't
- when the JVM multithreads, it starts on the main stack and then alternates between threads, doing the topmost command first, no matter which stack it is on

- to launch a new thread:
``` java
// 1. make the new runnable object (this is an interface)
// this is the "job" of the thread
Runnable threadJob = new MyRunnable();
// 2. make the thread and give the runnable to the thread, so the thread knows to read the runnable
Thread myThread = new Thread(threadJob);
// 3. start the thread
myThread.start();
```

### Runnable
- Runnable is an interface that contains the job of the thread, in the run() method
    - the run() method is the only method in Runnable
    - runnable is included in `java.lang.*`, so it does not need to be imported
- when you pass the Runnable to the thread (step 2 in the above example), you are simply telling the thread to run run()
``` java
public class MyRunnable implements Runnable {
    public void run() {
        go();
    }
    public void go() {
        System.out.println("This is what the new thread will run.");
    }
} 
```

### states of the thread
- a thread has three states: new, runnable, running, and blocked
- when the thread is new, it has not `start()`ed yet
- when the thread is runnable, it has been `start()`ed, but is not the top item on the stack
- when the thread is running, it is the top item on the stack
- when the thread is blocked, it is "temporarily not runnable"

- but most of the time, the thread should run smoothly from new -> runnable <-> running
- the thread scheduler is the one who decides what code is running and when, and cannot be changed
    - thus, you have no influence on whether or not the code runs, but most of the time, it should run smoothly
- but, you can put a thread to `sleep(amount of milliseconds)` to make a thread non-runnable, which ensures that other threads get run (hence making the scheduler more predictable)
- however, this makes a `InterruptedError`, which needs to be caught in a try/catch statement:
``` java
try {
 Thread.sleep(2000);
} catch(InterruptedException ex) {
 ex.printStackTrace();
}
```

### multi multi threading
- you can have more than two threads, just `setName()` for each thread
    - you can then run `getName()` for each thread

### concurrency issues! :(
- sadly, multithreading comes with its own set of issues
- basically, each thread acts as if there are no other threads
    - if you have two threads, then you have the chance of two threads acting on one object at once
- this leads to data corruption (e.g. overdrawing a bank account because the two owners of the account checked and withdrew money at the same time)

### synchronization
- you can fix concurrency issues by putting `sychronized` in the method heading
- `synchronized` prevents more than one thread from accessing a method at a time
- while a thread is completing one `synchronized` method, no other threads can access ANY synchronized method until the first thread is done
- this is because "locks" relate to the whole object, rather than the method
- make sure to avoid deadlock though, which is when you have two threads each holding the key towards the other thread, rendering them both useless
