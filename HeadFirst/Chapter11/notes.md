# notes

## JavaSound API
- a collection of classes and methods that play MIDI or sampled sounds

## exceptions
- if you call a method in a class that you didn't write, the method might have catastrophic effects on the rest of the code
    - so, you can test to see if it is risky by handling the exceptions that might happen, just in case
- if a method is known to be risky, then ```throws``` must be in the method declaration listing all possible exceptions
``` java
public void takeRisk() throws BadException, AnotherBadException {}
```
- a method should always catch what another method throws
- exceptions can be polymorphic, so you can declare exceptions using the superclass of the exception
    - for example: ClothingException would catch both ShirtException and PantsException
    - but you shouldn't go too broad, or you won't know what the actual issue is

## handling/declaring exceptions

### try/catch
``` java
try
{
     // do risky thing
}
//no code allowed here
catch(Exception ex)
{
     // try to recover
}
finally
{
    //no matter the result, this will run
}
```
- if the try block fails and there is an exception, the catch block will run, and no matter the result, the finally block will run
- if multiple exceptions, list all catches one after another
    - each exception should have its own catch block
    - each catch block should be ordered from most precise exception to least precise (PantsException would go before ClothingException)
- rules for try/catch
    - you cannot put code between the try and catch blocks
    - you must have a try block to have a catch or finally block
    - you must have a catch or finally block after after your try block
    - a try block with no catch must still declare the exception

### ducking exceptions
- if you don't want to handle an exception, you can duck it by declaring the method
- only delays the inevitable
