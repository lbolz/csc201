# notes
- this chapter is all about sorting, or, collection
- the ArrayList class does not have a `sort()` method, but there are other ways to sort lists
- there are also other key collection classes, such as:
    - TreeSet: keeps things sorted and without duplicates
    - HashMap: everything is sorted in key/value pairs
    - LinkedList
    - HashSet
    - LinkedHashMap
- to `sort()` a list, you can use the `java.util.Collections` class
- the `java.util.Collections` class has a variety of useful methods within it:
    - `copy()`, `emptyList()`, `sort()`, and more
- but, if you are sorting a class, sometimes the `Collections` class doesn't know how to sort
    - if you're sorting a bunch of `Duck`s, then what order should the `Duck`s be in? name? species? how does `Collections` know?

## generics
- with generics, you can create type-safe collections, which means you won't (or at least, shouldn't) run into issues where a `sort()` method doesn't know how to sort
- code that uses angle brackets `<>` involves generics
- most code relating to generics relates to collection
- example:
    ``` java
    // without generics
    ArrayList /* the type is <Object>, but is not explicitly written */ listName = new Arraylist();

    // with generics
    ArrayList<String> listName = new ArrayList<String>();
    ```
### using generic classes
- there are two main things that you need to look out for when using generic classes: the class declaration and the method declarations
``` java
// assuming you have the ArrayList class, and <E> is the object type that you declare when you make the list, then
public class ArrayList<E>{
    public boolean add(E object)
        // add the object to the list, if and only if it is of type E
}
```
