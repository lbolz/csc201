# notes
- objects have a behavior and a state
    - the behavior comes from the classes
    - the state comes within each object
        - thus, you need to be able to save the state of an object

## saving
### saving data
- depends on whether or not your data will be used by other programs
- if your data is used only by the code that generates it (you)
    - use serialization
        - this flattens the code
- if your data is used by other programs
    - write a plain text file
        - for example, a spreadsheet or database file
        - makes it easier for other programs to parse through
### saving state
- two ways to do it, depends on how readable/reliable you want your code to be
- writing the serialized character to a file
    - it won't make sense if you read it, but the computer will understand it very easily
- writing a plain text file with the pieces of state separated with commas
    - is much easier to read, but the computer may lose information

## data streams
- two main types of streams:
    - connection streams
        - represents the connections to a source or destination (file, socket, etc.)
    - chain streams
        - can't work unless they are attached to another stream
- most times, you need two streams to do something
- if needed, you can customize chains to do what you want with the data

## serializing

### serializing objects
- state is the value of an object's variable graph
- when you serialize an object, you save it's state
- an object's variable graph are all of the objects referenced to by the instance variables (and thus references of the references)

### serializing classes
- in order to make a class serializable, you need to implement ```Serializable```
- if a super-class IS-A serializable, then so is the sub-class, even if it is not explicitly declared
- a serializable class is one where it's objects can be serialized
- serializing is all or nothing, so either the entire object graph is serialized or none of it is
    - you will get a ```java.io.NotSerializableException```

### serializing example
``` java
FileOutputStream fileStream = new FileOutputStream("MyGame.ser");
ObjectOutputStream os = new ObjectOutputStream(fileStream);
os.writeObject(characterOne);
os.writeObject(characterTwo);
os.writeObject(characterThree);
os.close();
```

## deserialization

### deserializing objects
- works just like serialization in reverse
- the object is read from the stream, and the JVM attempts to read the objects type
- the objects are given space on the heap, but are not run
- the instance variables are given values

### deserialization example
``` java
FileInputStream fileStream = new FileInputStream("MyGame.ser");
ObjectInputStream os = new ObjectInputStream(fileStream);
Object one = os.readObject();
Object two = os.readObject();
Object three = os.readObject();GameCharacter elf = (GameCharacter) one;
GameCharacter troll = (GameCharacter) two;
GameCharacter magician = (GameCharacter) three;
os.close();
```

## transient variables
- if you want an instance variable to not be serialized, then mark it as transient

### transient variable example
``` java
class Chat implements Serializable
    {
    transient String currentID;
    public function void (){ /* code */ }
    }
```

## writing a string to a text file
- instead of using FileOutputStream, you use fileWriter to write a string instead of an object

### writing a string to text file example
``` java
import java.io.*;

class WriteAFile {
    public static void main (String[] args) {
        try {
            FileWriter writer = new FileWriter("Foo.txt");
            writer.write("hello foo!");
            writer.close();
        } catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}
```

### the java.io.File class
- represents a file on a disk, but not the contents of the file itself
- using a File object, you can:
    1. make a file
    ``` java
    File f = new File(“MyCode.txt”);
    ```
    2. make a new directory
    ``` java
    File dir = new File(“Chapter7”);
    dir.mkdir();
    ```
    3. list the contents of a directory
    ``` java
    if (dir.isDirectory()) {
        String[] dirContents = dir.list();
        for (int i = 0; i < dirContents.length; i++) {
            System.out.println(dirContents[i]);
        }
    }
    ```
    4. get an absolute path
    ``` java
    System.out.println(dir.getAbsolutePath());
    ```
    5. delete a file or directory
    ``` java
    boolean isDeleted = f.delete();
    ```

### buffers
- buffers are a temporary way of holding multiple variables together at once
    - if you didn't have a buffer, it'd be like shopping without a shopping cart
- make code more efficient
- using a buffer to write code to a string
    - the string is written to a BufferWriter, which is a chain stream of characters
    - when the buffer is full, it outputs a string of all of the strings added in the buffer
    - a FileWriter can be used to put that string into a text file

- BufferWriter example:
``` java
BufferedWriter writer = new BufferedWriter(new FileWriter(aFile));
```

## reading text files
- often read line by line (using a while loop) (see ReadAFile.java for a concrete example)

### splitting strings
- you can split a text file wherever there is a forward slash using ```.split("/")```
- splits wherever there is something inside the string that matches the separator

## version control
- you can't deserialize an object if you change the class after serializing the object
- thus, you need to make sure to either
    - have an old version of the class available
    - don't change the class after serializing
- overall, be mindful of changes and serialization

### serialVersionUID
- every time you serialize an object, it is given a new serialVersionUID
- this is what the JVM looks for when it attempts to deserialize a class
- if you think your class will evolve after you serialize an object, you must add the UID to the class as an instance variable
    - you can find the id by typing ```serialver filename```
