# notes

## static
- ```static``` allows you to run a method without an instance
- classes with static methods are often not instantiated
- ```private``` means only code within the class can invoke the method
    - basically, you can't call the method from outside the class
- static methods can't use non-static instance variables, or non-static methods
- you get one instance variable per instance, but only one static variable per class
    - for example: ```static count = 0;```

- static imports
    - exist to save typing (thus not needed)
    ``` java
    import java.lang.Math;
    System.out.println(“sqrt “ + Math.sqrt(2.0));
    ```

    - would turn into
    ``` java
    import java.lang.Math;
    out.println(“sqrt “ + sqrt(2.0));
    ```
    - thus, they make code confusing to read

## final
- ```final``` - variables are constant, methods can't be overridden, classes can't be extended
- final variables should be named in all caps
- you can initialize a final variable and then declare it
    - like this:
    ``` java
    public static final double BAR_SIGN;
    static
    {
        BAR_SIGN = (double) Math.random();
    }
    ```

## wrapping
- the wrapper classes are:
    - Boolean, Character, Byte, Short, Integer, Long, Float, Double
- wrapping a primitive
    - treating a primitive like an object
    - wrapping:
        ``` java
        int i = 288;
        Integer iWrap = new Integer(i);
        ```

## auto-boxing
- the process of automatically wrapping a primitive
- only does this after java version 5

## formatting strings
- two main parts
    - the formatting instructions
    - the string to be formatted
- a format specifier can have up to 5 parts
    ``` java
    % [argument number][flags][width][.precision] type
    ```
    - type is the only required specifier, and it must come last
- example
    ``` java
    String s = String.format(“%, d”, 1000000000);
    ```
    will give you: "1,000,000,000"

## dates
-  java.util.Calendar is an API used for data manipulation
    - it is an abstract class, so you must use the getInstance() method
- working with a calendar object
    - you can get and set the fields of a calendar (```get()``` and ```set()```)
    - you can change/add/subtract time to an object's fields
    - you can represent the calendar in milliseconds, which allows you to find elapsed time easily
- key fields/methods
    - fields
        - add(), get(), set(), roll(), etc.
    - methods
        - date, hour, millisecond, minute, month, year, etc.

# questions
1. none
