public class PlayerTester
{
    public static void main(String[] args)
    {
        System.out.println(Player.playerCount);
        Player first = new Player("Tiger Woods");
        System.out.println(Player.playerCount);

        //trying out string formatting with multiple string args
        int one = 20456654;
        double two = 100567890.248907;
        String s = String.format("The rank is %,d out of %,.2f", one, two);
        System.out.println(s);
    }
}
