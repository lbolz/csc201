# notes
- the layout manager controls the size and location of widgets in a GUI
- widgets are basically Swing components that extend ```javax.swing.JComponent```
- components can be nested

## how to make a GUI
1. make a window (a JFrame)
``` java
JFrame frame = new JFrame();
```

2. make a component
``` java
JButton button = new JButton("click me");

```

3. Add the component to the frame
``` java
frame.getContentPane().add(BorderLayout.EAST, button);
```

4. display it
``` java
frame.setSize(300,300);
frame.setVisible(true);
```

## layout managers
- a layout manager controls components
- there are three major layout managers
    - BorderLayout
    - FlowLayout
    - BoxLayout

### BorderLayout
- the default layout for a frame
- divides the background into 5 regions
- cares about 5 regions
    - north, south, east, west, center
- buttons change with the amount of text in them, but do not change the size of the frame
- once buttons in the north, south, east, and west have been created, the center button takes up whatever space is leftover

### FlowLayout
- the default layout for a panel
- components added left to right, wrapping to the next line when needed

### BoxLayout
- components added one at a time, top to bottom, wrapping when needed

## some swing components
- ```JTextField``` is a text field one line tall
``` java
JTextField field = new JTextField(20);
JTextField field = new JTextField("Your name");
```

- ```JTextArea``` is a text field that can be as large as needed
``` java
JTextArea text = new JTextArea(10,20);
```

- ```JCheckBox``` is a check box
``` java
JCheckBox check = new JCheckBox("Goes to 11");
```

- ```JList``` is a list
``` java
String [] listEntries = {"alpha", "beta", "gamma", "delta", "epsilon", "zeta", "eta", "theta "};
list = new JList(listEntries);
```
