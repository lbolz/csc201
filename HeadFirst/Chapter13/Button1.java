import javax.swing.*;
import java.awt.*;

public class Button1
{
    public static void main (String[] args)
    {
        Button1 gui = new Button1();
        gui.go();
    }

    public void go()
    {
        JFrame frame = new JFrame();
        JButton button = new JButton("click me like i have a lot of text");
        frame.getContentPane().add(BorderLayout.EAST, button);
        /* When it says BorderLayout.EAST, this is indicating that the layout
        format used should be BorderLayout, and the component should be placed
        in the EAST part of the frame.
        */
        frame.setSize(200,200);
        frame.setVisible(true);
    }
}
