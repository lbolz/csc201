public class Animal{
    public void makeNoise()
    {
        System.out.println("The animal makes a noise. ");
    }

    public void eat()
    {
        System.out.println("The animal eats. ");
    }

    public void sleep()
    {
        System.out.println("The animal sleeps. ");
    }

    public void roam()
    {
        System.out.println("The animal roams.");
    }
}
