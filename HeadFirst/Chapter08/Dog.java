public class Dog extends Canine implements Pet
{
    public void makeNoise()
    {
        System.out.println("The dog barks. ");
    }

    public void eat()
    {
        System.out.println("The dog eats dog food. ");
    }

    public void play()
    {
        System.out.println("The dog plays. ");
    }

    public void beFriendly()
    {
        System.out.print("The dog is friendly. ");
    }
}
