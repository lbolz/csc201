public class Wolf extends Canine
{
    public void makeNoise()
    {
        System.out.println("The wolf howls. ");
    }

    public void eat()
    {
        System.out.println("The wolf eats a deer leg. ");
    }
}
