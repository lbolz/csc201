public class Hippo extends Animal
{
    public void makeNoise()
    {
        System.out.println("The hippo wheezes. ");
    }

    public void eat()
    {
        System.out.println("The hippo eats foliage. ");
    }
}
