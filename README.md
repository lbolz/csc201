# csc201
## About This Repository
This is my repository containing my work from my CSC 201 class. 

The textbook used in this class was Head First Java, written by Bert Bates and Kathy Sierra.

All Headfirst chapters after chapter 9 have chapter notes within them.

## Useful Links / Other Related Repositories
- [Course Syllabus](http://ict.gctaa.net/sections/csc201/info/syllabus.pdf)
- [tdd_exercises](https://gitlab.com/lbolz/tdd_exercises)
